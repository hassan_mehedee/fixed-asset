const initialState = {
    _id: "",
    name: "",
    value: 0,
    purchasedate: new Date(),
    periodunit: "Month",
    deteriorationperiod: 12,
    status: true,
    targetaccount: "",
    fullaccountspath: [],
    description: "",
    sofref: "",

}

const initialSearch = {
    name: "",
    nameisused: false,
    purchasedatefrom: new Date(),
    purchasedateisused: false,
    purchasedateto: new Date(),
    purchasedatetoisused: false,
    description: "",
    descriptionisused: false,
    sofref: null,
    sofrefisused: false,
    targetaccount: null,
    targetaccountisused: false,
    status: false,
    statustype: null,
    statusisused: false,

}

export { initialState, initialSearch };