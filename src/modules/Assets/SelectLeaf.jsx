import { Grid, TextField, Typography } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { apiBaseUrl } from '../../config';

const viewModeView = "view";
const viewModeEdit = "edit";
const viewModeSave = "save";
const viewModeNew = "new";
const SelectLeaf = ({ openSuccess, openFailure, viewMode, levelSelected, setLevelSelected, childArray, setChildArray, self, setSelf, hasChildren, setHasChildren }) => {

    const [initialParents, setInitialParents] = useState([]);

    const makeInitialParents = () => {
        axios({
            method: 'GET',
            url: `${apiBaseUrl}/chartofacc/set_initial_parents`,
            headers: {
                "content-type": "application/json"
            }
        }).then(() => {
            handleGetInitialParents();
        }).catch((err) => {

        })
    }

    const handleGetInitialParents = () => {
        axios({
            method: 'POST',
            url: `${apiBaseUrl}/chartofacc/get_initial_parents`,
            headers: {
                "content-type": "application/json"
            },
            data: JSON.stringify({ isaccount: false, isaccountisused: true })
        }).then((res) => {
            const results = res.data.results;
            // console.log("get initial parent", results);
            if (results?.length > 0) {
                setInitialParents(results);
                // console.log(results);

            } else {
                makeInitialParents();
            }
        }).catch((err) => {
            openFailure(err?.response?.data || "Something went wrong!");
        })
    }

    const handleGetParentByID = (id, level) => {
        axios({
            method: 'POST',
            url: `${apiBaseUrl}/chartofacc/get_populated/${id}`,
            headers: {
                "content-type": "application/json"
            }
        }).then((res) => {
            if (res.data.results) {
                let len = res.data.results.childref.length;
                if (len > 0) {
                    setHasChildren(true)
                } else {
                    if (!res.data.results.isaccount) {
                        setHasChildren(false);
                    }
                }

                setSelf(res.data.results)
                let ChildArraylength = childArray.length - (level - 1);
                for (let index = 0; index < ChildArraylength; index++) {
                    childArray.pop();
                }

                const copyData = res.data.results.childref.filter(item => item?.isaccount === false)
                // console.log(copyData);

                setChildArray([...childArray, copyData]);

            } else {
                setSelf(null)
                setChildArray([]);
            }
        }).catch((err) => {
            openFailure(err?.response?.data || "Something went wrong!");
        })
    }

    const handleSelected = (level, id) => {
        // console.log(level);
        let levelLength = levelSelected.length - (level - 1);
        for (let index = 0; index < levelLength; index++) {
            levelSelected.pop()
        }
        setLevelSelected([...levelSelected, id]);
    }
    // console.log("All Levels", levelSelected);
    const handleRemove = (level) => {
        let levelLength = levelSelected.length - (level)
        for (let index = 0; index < levelLength; index++) {
            levelSelected.pop();
        }
        setLevelSelected([...levelSelected]);

        let ChildArraylength = childArray.length - (level);
        for (let index = 0; index < ChildArraylength; index++) {
            childArray.pop();
        }
        setChildArray([...childArray]);
    }

    // useEffect(() => {
    //     if (!hasChildren && isAccount) {
    //         console.log("Leaf Node");
    //     } else {
    //         console.log("Not Leaf Node");
    //     }
    // }, [hasChildren, isAccount])


    useEffect(() => {
        handleGetInitialParents();
    }, []);

    return (
        <div style={{ marginTop: "10px" }}>
            <Grid container>
                <Grid item xs={12}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} md={3} lg={3} >
                            <Autocomplete
                                disabled={viewMode === viewModeView ||
                                    viewMode === viewModeEdit}
                                size="small"
                                value={initialParents?.find(obj => levelSelected?.find(ls => ls === obj?._id)) || null}
                                onChange={(event, newValue) => {
                                    if (newValue === null) {
                                        handleRemove(0);
                                        setSelf(null);
                                    }
                                    else {
                                        handleGetParentByID(newValue?._id, newValue?.level);
                                        setSelf({ ...self, parentref: newValue?._id, name: newValue?.name });
                                        handleSelected(newValue?.level, newValue?._id);
                                    }
                                }}
                                id="controllable-states-demo"
                                fullWidth
                                options={initialParents}
                                getOptionLabel={option => option?.name}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        label="Choose Account"
                                        variant="outlined"
                                        // required
                                        error={Boolean(levelSelected[0] === self?._id && self?.childref?.length === 0 && !self?.isaccount)}
                                        helperText={Boolean(levelSelected[0] === self?._id && self?.childref?.length === 0 && !self?.isaccount) ? "This Node Does Not Have Any Account Node" : ""}
                                    />
                                )}
                            />
                        </Grid>
                        {
                            childArray.map((level, index) => {
                                return (
                                    level?.length > 0 ?
                                        <Grid item xs={12} md={3} lg={3}>
                                            <Autocomplete
                                                disabled={viewMode === viewModeView ||
                                                    viewMode === viewModeEdit}
                                                size="small"
                                                value={level?.find(obj => levelSelected?.find(ls => ls === obj?._id)) || null}
                                                onChange={(event, newValue) => {
                                                    // console.log(newValue);
                                                    if (newValue === null) {
                                                        handleRemove(index + 1);
                                                        setSelf(null)
                                                    }
                                                    else {
                                                        handleGetParentByID(newValue?._id, newValue?.level);
                                                        handleSelected(newValue?.level, newValue?._id);
                                                    }
                                                }}
                                                id="controllable-states-demo"
                                                fullWidth
                                                options={level.filter(item => item.isaccount === false)}
                                                // options={level}
                                                getOptionLabel={option => option?.name}
                                                renderInput={(params) => (
                                                    <TextField
                                                        {...params}
                                                        label="Choose Account"
                                                        variant="outlined"
                                                        required
                                                    // error={!Boolean(levelSelected[index + 1]) || Boolean(levelSelected[index + 1] === self?._id && self?.childref?.length === 0 && !self?.isaccount) || false}
                                                    // helperText={!Boolean(levelSelected[index + 1]) ? "Can't be Blank" : Boolean(levelSelected[index + 1] === self?._id && self?.childref?.length === 0 && !self?.isaccount) ? "You Can Create Fixed Assets on this Node" : ""}
                                                    />
                                                )}
                                            />
                                        </Grid> :
                                        <Grid item xs={12} md={3} lg={3} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                            {hasChildren ? <Typography variant="h5" style={{ color: "red" }}>This is not a leaf node!</Typography> : <Typography variant="h5" style={{ color: "green" }}>You have reached to the leaf node !</Typography>}

                                        </Grid>
                                )
                            })
                        }
                    </Grid>
                </Grid>
            </Grid>
        </div >
    );
};

export default SelectLeaf;