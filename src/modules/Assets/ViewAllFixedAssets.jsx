import { Box, Button, Grid, Paper, TextField, Tooltip, Switch, Typography, Chip, TablePagination } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { IoIosSearch, IoIosPaper } from "react-icons/io";
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import { MuiPickersUtilsProvider, KeyboardDatePicker, } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { initialSearch } from '../DB Models/asset';
import { allAssets, getSourceOfFund, statusUpdate } from '../API Models/asset';
import { notAccount } from '../API Models/asset';
import { EnhancedTableHead, getComparator, stableSort } from '../Resources/EnhancedTableHead.js';
import SuccessFailureAlert from '../Resources/SuccessFailureAlert.jsx';


const useStyles = makeStyles((theme) => ({
    root: {
        margin: theme.spacing(2)
    },
    paper: {
        padding: theme.spacing(2)
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
    textfieldClass: {
        '& .MuiOutlinedInput-input': {
            '&::-webkit-outer-spin-button, &::-webkit-inner-spin-button': {
                '-webkit-appearance': 'none',
            },
        }
    },
    tableHeadColors: {
        backgroundColor: "#aaa",
    }
}));
const headCells = [
    { id: 'actions', numeric: false, disablePadding: false, label: 'Actions', issortable: false },
    { id: 'name', numeric: false, disablePadding: false, label: 'Name', issortable: true },
    { id: 'value', numeric: false, disablePadding: false, label: 'Value', issortable: true },
    { id: 'deterValue', numeric: false, disablePadding: false, label: 'Deterioration Value', issortable: true },
    { id: 'currentValue', numeric: false, disablePadding: false, label: 'Current Value', issortable: true },
    { id: 'elapsedTimePercents', numeric: false, disablePadding: false, label: 'Elapsed Time (%)', issortable: true },
    { id: 'purchasedate', numeric: false, disablePadding: false, label: 'Purchase Date', issortable: true },
    { id: 'deteriorationperiod', numeric: false, disablePadding: false, label: 'Deterioration Period', issortable: true },
    { id: 'description', numeric: false, disablePadding: false, label: 'Description', issortable: false },
    { id: 'targetaccount', numeric: false, disablePadding: false, label: 'Account', issortable: false },
    { id: 'status', numeric: false, disablePadding: false, label: 'Status', issortable: true },
];
const ViewAllFixedAssets = ({ setValue, setSentAsset }) => {
    /*------------All States --------------- */
    const classes = useStyles();
    const [searchString, setSearchString] = useState(initialSearch);
    const [fixedAssets, setFixedAssets] = useState([]);
    const [totalAssetsValue, setTotalAssetsValue] = useState(0.0);
    const [category, setCategory] = useState([]);
    const [source, setSource] = useState([]);
    const [openSuccessAlert, setOpenSuccessAlert] = useState(false);
    const [openFailureAlert, setOpenFailureAlert] = useState(false);
    const [message, setMessage] = useState("");
    // table states
    const [order, setOrder] = useState('asc');
    const [orderBy, setOrderBy] = useState('');
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(100);
    /*------------All States ends--------------- */

    const openFailure = (message) => {
        setMessage("");
        setOpenSuccessAlert(false);
        setOpenFailureAlert(true);
        setMessage(message);
    };

    // table functions start
    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };


    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };
    // table functions ends

    /*------------API call Functions start--------------- */
    // get all accounts
    const getAllAssets = (dataString) => {
        // console.log(dataString);
        allAssets(dataString).then(res => {
            if (res[0]) {
                let newRes = [];
                for (let i = 0; i < res[1].length; i++) {
                    // equation 1 : how many months the fixed asset has been kept in hand
                    let holdingFixedAssetValue = Math.floor((new Date() - new Date(res[1][i].purchasedate)) / (1000 * 60 * 60 * 24 * 30));
                    // equation 2 : calculation of deterioration in month
                    let deterInMonth = 0;
                    if (res[1][i].periodunit === 'Month') {
                        deterInMonth = parseInt(res[1][i].deteriorationperiod)
                    } else {
                        deterInMonth = parseInt(res[1][i].deteriorationperiod) * 12
                    }
                    // calculation of month value = asset value/ deterioration in month
                    let monthValue = Math.floor(parseInt(res[1][i].value) / deterInMonth);
                    // deterioration value calculation = equation1 * equation2
                    let deterValue = Math.floor(holdingFixedAssetValue * monthValue);
                    // current value = asset value - deterValue
                    let currentValue = Math.floor(parseInt(res[1][i].value) - deterValue);
                    // elapsed time in percentage = (equation1/deterInMonth)*100
                    let elapsedTimePercents = Math.floor((holdingFixedAssetValue / deterInMonth) * 100);
                    res[1][i].deterValue = deterValue;
                    res[1][i].currentValue = currentValue;
                    res[1][i].elapsedTimePercents = elapsedTimePercents;
                    newRes.push(res[1][i]);

                }
                newRes ? setFixedAssets(newRes) : setFixedAssets([]);
            } else {
                openFailure(res[1]);
            }
        });
    };

    // search function
    const searchAsset = () => {
        const copyData = { ...searchString };
        copyData.sofref = searchString?.sofref?._id;
        copyData.targetaccount = searchString?.targetaccount?._id;
        const newDateFrom = new Date(searchString?.purchasedatefrom);
        newDateFrom.setHours(0, 0, 0, 0);
        const newDateTo = new Date(searchString?.purchasedateto);
        newDateTo.setHours(23, 59, 59, 999);
        copyData.purchasedatefrom = newDateFrom;
        copyData.purchasedateto = newDateTo;

        getAllAssets(copyData);
    }


    const getCategory = () => {
        notAccount().then(res => {
            if (res[0]) {
                res[1] ? setCategory(res[1]) : setCategory([]);
            } else {
                openFailure(res[1])
            }
        });
    }
    const getAllSof = () => {
        const filter = {
            status: true,
            statusisused: true,
        }
        getSourceOfFund(filter).then((res) => {
            if (res[0]) {
                res[1] ? setSource(res[1]) : setSource([]);
            } else {
                openFailure(res[1]);
            }
        })
    }


    /*------------API call Functions end--------------- */

    /*------------Handler Functions starts--------------- */
    // status update function
    const handleStatusChange = (id, status) => {
        const isActive = status ? "active" : "inactive";
        statusUpdate(id, isActive).then((res) => {
            if (res[0]) {
                getAllAssets();
            } else {
                openFailure(res[1]);
            }
        });
    };
    // clear button click function
    const clearSearch = () => {
        setSearchString(initialSearch);
        getAllAssets();
    }

    /*------------Handler Functions ends--------------- */

    // useEffect for fetching data on page load
    useEffect(() => {
        getAllAssets();
        getCategory();
        getAllSof();
    }, [])

    useEffect(() => {
        let sum = 0
        for (let i = 0; i < fixedAssets.length; i++) {
            const element = fixedAssets[i];
            sum = sum + element?.value
        }
        setTotalAssetsValue(sum)
    }, [fixedAssets])

    return (
        <Box className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={12} md={12}>
                    <Paper elevation={2} className={classes.paper}>
                        <Grid container spacing={1}>
                            {/* search by account name field starts */}
                            <SuccessFailureAlert
                                openSuccessAlert={openSuccessAlert}
                                message={message}
                                openFailureAlert={openFailureAlert}
                                setopenSuccessAlert={setOpenSuccessAlert}
                                setopenFailureAlert={setOpenFailureAlert}
                            />
                            <Grid item xs={6} md={3}>
                                <TextField
                                    value={searchString.name}
                                    onChange={(e) => setSearchString({ ...searchString, name: e.target.value, nameisused: true })}
                                    fullWidth
                                    size="small"
                                    label="Search by Name"
                                    variant="outlined"
                                    id="outlined-basic"
                                />
                            </Grid>
                            {/* search by account name field ends */}
                            {/* search by description starts */}
                            <Grid item xs={6} md={3}>
                                <TextField
                                    value={searchString?.description}
                                    onChange={(e) => {
                                        console.log(e.target.value);
                                        setSearchString({ ...searchString, description: e.target.value, descriptionisused: true });
                                    }}
                                    fullWidth
                                    size="small"
                                    label="Search by Description"
                                    variant="outlined"
                                    id="outlined-basic"
                                />
                            </Grid>
                            {/* search by description ends */}
                            {/* search by purchase date from field */}
                            <Grid item xs={6} md={3}>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <KeyboardDatePicker
                                        fullWidth
                                        size='small'
                                        autoOk
                                        variant="inline"
                                        inputVariant="outlined"
                                        label="Search by Purchase Date From"
                                        format="MM/dd/yyyy"
                                        value={searchString?.purchasedatefrom}
                                        onChange={(date) => {
                                            setSearchString({ ...searchString, purchasedatefrom: date, purchasedateisused: Boolean(date) });
                                        }}
                                        KeyboardButtonProps={{
                                            'aria-label': 'change date',
                                        }}
                                    />
                                </MuiPickersUtilsProvider>
                            </Grid>
                            {/* search by purchase date to field */}
                            <Grid item xs={6} md={3}>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <KeyboardDatePicker
                                        fullWidth
                                        size='small'
                                        autoOk
                                        variant="inline"
                                        inputVariant="outlined"
                                        label="Search by Purchase Date To"
                                        format="MM/dd/yyyy"

                                        value={searchString?.purchasedateto}
                                        onChange={(date) => {
                                            setSearchString({ ...searchString, purchasedateto: date, purchasedatetoisused: true })
                                        }}
                                        KeyboardButtonProps={{
                                            'aria-label': 'change date',
                                        }}
                                    />
                                </MuiPickersUtilsProvider>
                            </Grid>
                            {/* search by sof field starts */}
                            <Grid item xs={6} md={3}>
                                <Autocomplete
                                    fullWidth
                                    disablePortal
                                    id="combo-box-demo"
                                    value={searchString?.sofref}
                                    options={source}
                                    getOptionLabel={(option) => option.name}
                                    renderInput={(params) => (
                                        <TextField {...params} variant="outlined" label="SoF" />
                                    )}
                                    size="small"
                                    onChange={(e, newValue) => {
                                        console.log("selected SoF", newValue);
                                        setSearchString({
                                            ...searchString,
                                            sofref: newValue,
                                            sofrefisused: Boolean(newValue)
                                        });
                                    }}
                                />
                            </Grid>
                            {/* search by sof field ends */}
                            {/* search by category starts */}
                            <Grid item xs={6} md={3}>
                                <Autocomplete
                                    fullWidth
                                    disablePortal
                                    id="combo-box-demo"
                                    value={searchString?.targetaccount}
                                    options={category}
                                    getOptionLabel={(option) => option.name}
                                    renderInput={(params) => (
                                        <TextField {...params} variant="outlined" label="Search by Category" />
                                    )}
                                    size="small"
                                    onChange={(e, newValue) => {
                                        console.log(newValue._id);
                                        setSearchString({ ...searchString, targetaccount: newValue, targetaccountisused: Boolean(newValue) });
                                    }}
                                />
                            </Grid>
                            {/* search by category ends */}
                            {/* search by status starts */}
                            <Grid item xs={6} md={3}>
                                <Autocomplete
                                    fullWidth
                                    disablePortal
                                    id="combo-box-demo"
                                    value={searchString?.statustype}
                                    options={["Active", "Inactive"]}
                                    getOptionLabel={(option) => option}
                                    renderInput={(params) => (
                                        <TextField {...params} variant="outlined" label="Search by Status" />
                                    )}
                                    size="small"
                                    onChange={(e, newValue) => {
                                        setSearchString({ ...searchString, status: newValue === "Active" ? true : false, statusisused: Boolean(newValue), statustype: newValue });
                                    }}
                                />
                            </Grid>
                            {/* search by status ends */}

                            <Grid item xs={3} md={3} lg={3}>
                                <Tooltip title="Search Campaign">
                                    <Button
                                        style={{ marginRight: "1rem" }}
                                        onClick={() => searchAsset()}
                                        variant="contained"
                                        color="primary"
                                    >
                                        <IoIosSearch fontSize="1.5rem" />
                                    </Button>
                                </Tooltip>
                                <Button
                                    onClick={() => clearSearch()}
                                    variant="contained"
                                    sx={{ backgroundColor: "secondary" }}
                                    size="medium"
                                >
                                    X
                                </Button>
                            </Grid>
                        </Grid>

                    </Paper>
                </Grid>
                {/* table grid starts from here */}
                <Grid item={12} md={12}>
                    <Paper elevation={2}>
                        <TableContainer style={{ maxHeight: "70vh" }}>
                            <Table
                                aria-labelledby="tableTitle"
                                size='small'
                                aria-label="enhanced table"
                                stickyHeader
                            >
                                <EnhancedTableHead
                                    order={order}
                                    orderBy={orderBy}
                                    onRequestSort={handleRequestSort}
                                    headCells={headCells}
                                />
                                <TableBody>
                                    {stableSort(fixedAssets, getComparator(order, orderBy))
                                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                        .map((row, index) => {
                                            return (
                                                <TableRow
                                                    hover
                                                    role="checkbox"
                                                    tabIndex={-1}
                                                    key={index}
                                                >
                                                    <TableCell>
                                                        <Box style={{
                                                            display: "flex",
                                                            alignItems: "center",
                                                        }}>
                                                            <Tooltip title="Manage Fixed Asset">
                                                                <Button
                                                                    onClick={() => {
                                                                        setValue(0);
                                                                        setSentAsset(row);
                                                                    }}
                                                                    color="primary"
                                                                    style={{ marginRight: 0.5 }}
                                                                    variant="contained"
                                                                    size="small"
                                                                >
                                                                    <IoIosPaper />
                                                                </Button>
                                                            </Tooltip>
                                                            <Switch
                                                                size="small"
                                                                color="primary"
                                                                checked={row?.status}
                                                                onChange={(e) => {
                                                                    console.log(e.target.checked)
                                                                    handleStatusChange(row?._id, e.target.checked)
                                                                }
                                                                }
                                                            />
                                                        </Box>
                                                    </TableCell>
                                                    <TableCell align="left" component="th" scope="row">
                                                        {row?.name}
                                                    </TableCell>
                                                    <TableCell align="left" style={{ backgroundColor: "#b3e5fc", fontWeight: "bold" }}>{row?.value}</TableCell>
                                                    <TableCell align="left" style={{ backgroundColor: "#e57373", fontWeight: "bold" }}>{row?.deterValue}</TableCell>
                                                    <TableCell align="left" style={{ backgroundColor: "#4db6ac", fontWeight: "bold" }}>{row?.currentValue}</TableCell>
                                                    <TableCell align="left">
                                                        {
                                                            <Chip
                                                                size="small"
                                                                label={`${row?.elapsedTimePercents}%`}
                                                                clickable
                                                                style={
                                                                    row?.elapsedTimePercents < 70 ? { backgroundColor: "#43a047", color: "white" } : (row?.elapsedTimePercents >= 70 && row?.elapsedTimePercents <= 85) ? { backgroundColor: "#ffa726", color: "white" } : { backgroundColor: "#f44336", color: "white" }
                                                                }

                                                            />
                                                        }
                                                    </TableCell>
                                                    <TableCell align="left">{row?.purchasedate.slice(0, 10)}</TableCell>
                                                    <TableCell align="left">{row?.deteriorationperiod} {row?.periodunit}</TableCell>
                                                    {
                                                        row?.description.length >= 30 ? <TableCell align="left">{`${row?.description.slice(0, 30)}...`}</TableCell> : <TableCell align="left">{row?.description}</TableCell>
                                                    }
                                                    <TableCell align="left">{row?.targetaccount.name}</TableCell>
                                                    <TableCell align="left"><span style={row?.status ? { color: "green", fontWeight: "bold" } : { color: "red", fontWeight: "bold" }}>{row?.status ? "Active" : "Inactive"}</span></TableCell>
                                                </TableRow>
                                            );
                                        })}
                                    <TableRow>
                                        <TableCell></TableCell>
                                        <TableCell></TableCell>
                                        <TableCell colSpan={2}><Typography variant="h6"><b>Total</b>: {totalAssetsValue.toFixed(2)}</Typography></TableCell>
                                        <TableCell></TableCell>
                                        <TableCell></TableCell>
                                        <TableCell></TableCell>
                                        <TableCell></TableCell>
                                        <TableCell></TableCell>
                                        <TableCell></TableCell>
                                        <TableCell></TableCell>
                                        {/* <TableCell></TableCell> */}
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </TableContainer>
                        <TablePagination
                            rowsPerPageOptions={[100, 300, 1000]}
                            component="div"
                            count={fixedAssets.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                        />
                    </Paper>
                </Grid>

            </Grid>
        </Box>
    );
};

export default ViewAllFixedAssets;