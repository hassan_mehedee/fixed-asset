import { Box, Button, Grid, Paper, FormControlLabel, Switch, TextField, } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker, } from '@material-ui/pickers';
import 'date-fns';
import { initialState } from '../DB Models/asset';
import { useEffect } from 'react';
import { modifyAsset, newAssetCreation, getSourceOfFund } from '../API Models/asset';
import SelectLeaf from './SelectLeaf';
import SuccessFailureAlert from '../Resources/SuccessFailureAlert';

const useStyles = makeStyles((theme) => ({
    root: {
        margin: theme.spacing(2)
    },
    paper: {
        padding: theme.spacing(2)
    },
    button: {
        marginRight: theme.spacing(1)
    },
}));
// variables for disabling buttons on the top
const viewModeView = "view";
const viewModeEdit = "edit";
const viewModeSave = "save";
const viewModeNew = "new";

const ManageFixedAssets = ({ sentAsset, setSentAsset }) => {
    /* ---------start of states ----------*/
    const classes = useStyles();
    const [viewMode, setViewMode] = useState(viewModeView);
    const [openSuccessAlert, setOpenSuccessAlert] = useState(false);
    const [openFailureAlert, setOpenFailureAlert] = useState(false);
    const [message, setMessage] = useState("");

    const [currentAsset, setCurrentAsset] = useState(initialState);

    const [accountSelf, setAccountSelf] = useState({});
    const [childCategory, setChildCategory] = useState([]);
    const [productPath, setProductPath] = useState([]);
    const [hasChildren, setHasChildren] = useState(false);

    const [source, setSource] = useState([]);

    const [emptyError, setEmptyError] = useState(1);
    const [numFieldError, setNumFieldError] = useState(1);
    // flag for making sure that use has come from the modify menu
    const [confirmSent, setConfirmSent] = useState(0);
    const [shouldDisable, setShouldDisable] = useState(false);
    /* ---------end of states ----------*/

    // success and failure message
    const openFailure = (message) => {
        setMessage("");
        setOpenSuccessAlert(false);
        setOpenFailureAlert(true);
        setMessage(message);
    };
    const openSuccess = (message) => {
        setMessage("");
        setOpenSuccessAlert(true);
        setOpenFailureAlert(false);
        setMessage(message);
    };
    // validation
    const validateAsset = () => {
        if (!currentAsset?.name) return "Please Insert Your Name";
        if (!currentAsset?.value) return "Please Insert a value";
        if (!currentAsset?.purchasedate) return "Please Pick a Date";
        if (hasChildren) return "You can't create asset with this account as this is not a child node!";
        return "";
    };

    /*-----------handler functions starts---------- */
    const clear = () => {
        setEmptyError(1);
        setCurrentAsset(initialState);
        setChildCategory([]);
        setViewMode(viewModeView);
        setProductPath([]);
        setSentAsset(null);
        setConfirmSent(0);
    }

    const clearAlert = () => {
        setOpenSuccessAlert(false);
        setOpenFailureAlert(false);
        setMessage("");
    }

    /*-----------API Call Functions---------- */
    // source of fund function
    const getAllSof = (value) => {
        const filter = {
            status: true,
            statusisused: true,
            currentamount: value,
            currentamountisused: true,
        }
        getSourceOfFund(filter).then((res) => {
            if (res[0]) {
                res[1] ? setSource(res[1]) : setSource([]);
            } else {
                openFailure(res[1]);
            }
        })
    }
    /*-----------End of API Call Functions---------- */

    const createAsset = () => {
        currentAsset.fullaccountspath = productPath;
        currentAsset.targetaccount = productPath[productPath.length - 1];
        currentAsset.sofref = currentAsset?.sofref?._id;
        newAssetCreation(currentAsset).then((res) => {
            if (res[0]) {
                openSuccess(res[1]);
                clear()
            } else {
                openFailure(res[1]);
            }
        })
    }

    const handleModify = () => {
        const copyData = { ...currentAsset };
        copyData.fullaccountspath = productPath;
        copyData.targetaccount = productPath[productPath.length - 1];
        copyData.sofref = currentAsset?.sofref?._id;

        modifyAsset(copyData).then((res) => {
            if (res[0]) {
                openSuccess(res[1]);
                clear();
            } else {
                openFailure(res[1]);
            }
        })
    }
    const saveAsset = () => {
        let err = validateAsset();
        if (err !== "") {
            openFailure(err);
            return;
        } else {
            clearAlert()
            if (viewMode === viewModeSave) {
                handleModify();
            } else if (viewMode === viewModeNew) {
                createAsset();
            }
        }
    }

    // when clicking on the manage asset button on the ViewAsset page it's redirects to Manage Asset page and this data loads
    useEffect(() => {
        if (sentAsset) {
            setCurrentAsset(sentAsset);
            setConfirmSent(1);
            let path = [];
            let path2 = [];
            for (let i = 0; i < sentAsset?.fullaccountspath?.length; i++) {
                path.push(sentAsset?.fullaccountspath[i]?._id);
                const element = sentAsset?.fullaccountspath[i];
                let arr = [element];
                if (i !== 0) {
                    path2.push(arr);
                }
            }
            setProductPath(path);
            setChildCategory(path2);
            setViewMode(viewModeEdit);
            setSentAsset(null);
        }
    }, []);
    // load data with page load

    // load the source of fund data after confirming that it is for modifying.this source is dependent on the value of current asset's value

    useEffect(() => {
        getAllSof(currentAsset?.value)
        let valueToAddress = currentAsset?.sofref?._id;
        if (confirmSent && Number(valueToAddress) !== 0) {
            setShouldDisable(true);
        } else {
            setShouldDisable(false);
        }
    }, [currentAsset?.value])

    /*-----------handler functions ends---------- */
    return (
        <Box className={classes.root}>
            <Grid container spacing={2}>
                {/* grid item for buttons starts*/}
                <Grid item xs={12} md={12} lg={12}>
                    <Button
                        size='small'
                        onClick={() => {
                            setViewMode(viewModeNew);
                            clearAlert();
                        }}
                        disabled={viewMode !== viewModeView}
                        className={classes.button} variant="contained" color="secondary">
                        CREATE NEW
                    </Button>
                    <Button size='small' color="secondary"
                        onClick={() => setViewMode(viewModeSave)}
                        className={classes.button} variant="contained" disabled={viewMode !== viewModeEdit}>
                        EDIT
                    </Button>
                    <Button size='small' color="primary"
                        onClick={() => saveAsset()}
                        className={classes.button}
                        variant="contained"
                        disabled={viewMode === viewModeView || viewMode === viewModeEdit}>
                        SAVE
                    </Button>
                    <Button className={classes.button} size='small'
                        onClick={() => {
                            clear();
                            clearAlert();
                        }} variant="contained">
                        CANCEL
                    </Button>
                </Grid>
                <SuccessFailureAlert
                    openSuccessAlert={openSuccessAlert}
                    message={message}
                    openFailureAlert={openFailureAlert}
                    setopenSuccessAlert={setOpenSuccessAlert}
                    setopenFailureAlert={setOpenFailureAlert}
                />
                {/* grid item for buttons ends*/}
                {/* paper grid starts here */}
                <Grid item xs={12}>
                    <Paper elevation={3} className={classes.paper}>
                        <Grid container spacing={1}>
                            {/* status switch  */}
                            <Grid
                                item
                                xs={12}
                                style={{ display: "flex", justifyContent: "center" }}
                            >
                                {" "}
                                <FormControlLabel
                                    disabled={
                                        viewMode === viewModeView || viewMode === viewModeEdit
                                    }
                                    control={
                                        <Switch
                                            size="small"
                                            checked={currentAsset?.status}
                                            onChange={(e) => {
                                                setCurrentAsset({ ...currentAsset, status: e.target.checked })
                                            }
                                            }
                                            color="primary"
                                        />
                                    }
                                    label="Active"
                                />
                            </Grid>
                            {/* name field starts */}
                            <Grid item xs={6}>
                                <TextField
                                    error={!emptyError ? true : false}
                                    onFocus={() => { setEmptyError(1); }}
                                    onBlur={() => {
                                        if (!currentAsset?.name) {
                                            setEmptyError(0);
                                        }
                                    }}
                                    disabled={
                                        viewMode === viewModeView || viewMode === viewModeEdit
                                    }
                                    helperText={!emptyError ? "name required" : ""}
                                    required
                                    id="outlined-basic"
                                    label="Name"
                                    variant="outlined"
                                    fullWidth
                                    size="small"
                                    value={currentAsset?.name}
                                    onChange={(e) => { setCurrentAsset({ ...currentAsset, name: e.target.value }) }}
                                />
                            </Grid>
                            {/* name field ends */}
                            {/* value field starts */}
                            <Grid item xs={3}>
                                {
                                    confirmSent ?
                                        <TextField
                                            error={!numFieldError ? true : false}
                                            onFocus={() => { setNumFieldError(1); }}
                                            onBlur={() => {
                                                if (!currentAsset?.value) {
                                                    setNumFieldError(0);
                                                }
                                            }}
                                            disabled={viewMode === viewModeEdit || shouldDisable}
                                            type="number"
                                            inputProps={{ min: 0 }}
                                            required
                                            id="outlined-basic"
                                            label="Modify Value" //this label must be changed to value
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                            value={currentAsset?.value}
                                            helperText={!numFieldError ? "value must be greater than 0" : ""}
                                            onChange={(e) => {
                                                setCurrentAsset({ ...currentAsset, value: parseFloat(e.target.value) });
                                            }}
                                        />
                                        :
                                        <TextField
                                            error={!numFieldError ? true : false}
                                            onFocus={() => { setNumFieldError(1); }}
                                            onBlur={() => {
                                                if (!currentAsset?.value) {
                                                    setNumFieldError(0);
                                                }
                                                getAllSof(currentAsset?.value);
                                            }}
                                            disabled={
                                                viewMode === viewModeView || viewMode === viewModeEdit
                                            }
                                            type="number"
                                            inputProps={{ min: 0 }}
                                            required
                                            id="outlined-basic"
                                            label="Value"
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                            value={currentAsset?.value}
                                            helperText={!numFieldError ? "value must be greater than 0" : ""}
                                            onChange={(e) => {
                                                setCurrentAsset({ ...currentAsset, value: parseFloat(e.target.value) });
                                            }}
                                        />
                                }

                            </Grid>
                            {/* value field ends */}
                            {/* purchase date field starts*/}
                            <Grid item xs={6}>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <KeyboardDatePicker
                                        required
                                        fullWidth
                                        disabled={viewMode === viewModeView || viewMode === viewModeEdit}
                                        maxDate={new Date()}
                                        size='small'
                                        autoOk
                                        variant="inline"
                                        inputVariant="outlined"
                                        label="Purchase Date"
                                        format="MM/dd/yyyy"
                                        value={currentAsset?.purchasedate}
                                        onChange={(date) => {
                                            setCurrentAsset({ ...currentAsset, purchasedate: date })
                                        }}
                                        KeyboardButtonProps={{
                                            'aria-label': 'change date',
                                        }}
                                    />
                                </MuiPickersUtilsProvider>
                            </Grid>
                            {/* purchase date field ends*/}
                            {/* SOF field starts */}
                            <Grid item xs={6}>
                                <Autocomplete
                                    disabled={viewMode === viewModeView || viewMode === viewModeEdit || shouldDisable || !currentAsset.value}
                                    fullWidth
                                    disablePortal
                                    id="combo-box-demo"
                                    value={currentAsset?.sofref}
                                    options={source}
                                    getOptionLabel={(option) => option?.name}
                                    renderInput={(params) => (
                                        <TextField {...params} variant="outlined" label="SoF" />
                                    )}
                                    size="small"
                                    onChange={(e, newValue) => {
                                        setCurrentAsset({
                                            ...currentAsset,
                                            sofref: newValue,
                                        });
                                    }}
                                />
                            </Grid>
                            {/* SOF field starts */}
                            {/* D.P field starts */}
                            <Grid item xs={3}>
                                <TextField
                                    disabled={
                                        viewMode === viewModeView || viewMode === viewModeEdit
                                    }
                                    type="number"
                                    inputProps={{ min: 0 }}
                                    id="outlined-basic"
                                    label="Deterioration Period"
                                    variant="outlined"
                                    fullWidth
                                    size="small"
                                    value={currentAsset?.deteriorationperiod}
                                    onChange={(e) => {
                                        setCurrentAsset({ ...currentAsset, deteriorationperiod: parseInt(e.target.value) });
                                    }}
                                />
                            </Grid>
                            {/* D.P field ends */}
                            {/* Period field starts */}
                            <Grid item xs={3}>
                                <Autocomplete
                                    disabled={
                                        viewMode === viewModeView ||
                                        viewMode === viewModeEdit
                                    }
                                    fullWidth
                                    disablePortal
                                    id="combo-box-demo"
                                    value={currentAsset?.periodunit}
                                    options={['Month', 'Year']}
                                    getOptionLabel={(option) => option}
                                    renderInput={(params) => (
                                        <TextField {...params} variant="outlined" label="Period" />
                                    )}
                                    size="small"
                                    onChange={(e, newValue) => {
                                        setCurrentAsset({
                                            ...currentAsset,
                                            periodunit: newValue,
                                        });
                                    }}
                                />
                            </Grid>
                            {/* Period field ends */}
                            {/* Dynamic Category Autocomplete Fields starts */}
                            <Grid item xs={12}>
                                <SelectLeaf
                                    hasChildren={hasChildren}
                                    setHasChildren={setHasChildren}
                                    self={accountSelf}
                                    setSelf={setAccountSelf}
                                    levelSelected={productPath}
                                    setLevelSelected={setProductPath}
                                    viewMode={viewMode}
                                    childArray={childCategory}
                                    setChildArray={setChildCategory}
                                />
                            </Grid>
                            {/* description field */}
                            <Grid item xs={12}>
                                <TextField
                                    disabled={
                                        viewMode === viewModeView || viewMode === viewModeEdit
                                    }
                                    multiline
                                    rows={4}
                                    id="outlined-basic"
                                    label="Description"
                                    variant="outlined"
                                    fullWidth
                                    size="small"
                                    value={currentAsset?.description}
                                    onChange={(e) => {
                                        setCurrentAsset({ ...currentAsset, description: e.target.value });
                                    }}
                                />
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>

                {/* paper grid ends here */}
            </Grid >
        </Box >
    );
};

export default ManageFixedAssets;