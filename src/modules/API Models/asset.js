import axios from "axios";
import { apiBaseUrl } from "../../config";


// function for getting all the fixed assets
const allAssets = async (filter) => {
    try {
        let response = await axios({
            method: "POST",
            url: `${apiBaseUrl}/fixed_assets/get_all_populated`,
            headers: { "Content-Type": "application/json" },
            data: JSON.stringify(filter),
        });
        return [true, response?.data?.result];
    } catch (error) {
        return [false, error.response];
    }
}

// create new asset function
const newAssetCreation = async (newAsset) => {
    try {
        let response = await axios({
            method: "POST",
            url: `${apiBaseUrl}/fixed_assets/new`,
            headers: { "Content-Type": "application/json" },
            data: JSON.stringify(newAsset),
        })
        return [true, response?.data?.InsertedID ? "Fixed Asset Created Successfully" : "Failed"];
    } catch (err) {
        return [false, err?.response?.data];
    }
};

// modify asset function
const modifyAsset = async (modifiedAsset) => {
    try {
        let response = await axios({
            method: "PUT",
            url: `${apiBaseUrl}/fixed_assets/modify/${modifiedAsset?._id}`,
            headers: { "Content-Type": "application/json" },
            data: JSON.stringify(modifiedAsset),
        });
        return [true, response?.data];
    } catch (err) {
        return [false, err?.response?.data];
    }
};
// function for changing status

const statusUpdate = async (id, status) => {
    try {
        let response = await axios({
            method: "PUT",
            url: `${apiBaseUrl}/fixed_assets/set_status/${id}/${status}`,
            headers: { "Content-Type": "application/json" },
        });
        return [true, response?.data?.result];
    } catch (error) {
        return [false, error?.response];
    }
}

//get the data for category autocomplete
const notAccount = async () => {
    let filter = {
        isaccount: false,
        isaccountisused: true
    }
    try {
        let response = await axios({
            method: "POST",
            url: `${apiBaseUrl}/chartofacc/get_all_populated_with_directory`,
            headers: { "Content-Type": "application/json" },
            data: JSON.stringify(filter),
        })
        return [true, response?.data?.result];
    } catch (error) {
        return [false, error?.response];
    }
}

// source of fund fund

const getSourceOfFund = async (filter) => {
    try {
        let response = await axios({
            method: "POST",
            url: `${apiBaseUrl}/sof/get_all_populated`,
            headers: { "Content-Type": "application/json" },
            data: JSON.stringify(filter),
        })
        return [true, response?.data?.result];
    } catch (error) {
        return [false, error?.response];
    }
}

export { allAssets, statusUpdate, newAssetCreation, modifyAsset, notAccount, getSourceOfFund };