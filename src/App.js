
import FixedAssetsContainer from './modules/Assets/FixedAssetsContainer';

function App() {
  return (
    <div>
      <FixedAssetsContainer />
    </div>
  );
}

export default App;
